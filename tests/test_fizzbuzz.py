import unittest
from unittest.mock import patch
from fizzbuzz import fizzbuzz
from fizzbuzz import load_fizzbuzz
from fizzbuzz import convert_result
from fizzbuzz import submit_result
from fizzbuzz import run


class TestFizzBuzz(unittest.TestCase):

    def test_true_is_true(self):
        self.assertEqual(True, True)

    def test_fizzbuzz_of_1_is_1(self):
        result = fizzbuzz(1)
        self.assertEqual(result, 1)

    def test_fizzbuzz_of_2_is_2(self):
        result = fizzbuzz(2)
        self.assertEqual(result, 2)

    def test_fizzbuzz_of_3_is_fizz(self):
        result = fizzbuzz(3)
        self.assertEqual(result, 'fizz')

    def test_fizzbuzz_of_5_is_buzz(self):
        result = fizzbuzz(5)
        self.assertEqual(result, 'buzz')

    def test_fizzbuzz_of_6_is_fizz(self):
        result = fizzbuzz(6)
        self.assertEqual(result, 'fizz')

    def test_fizzbuzz_of_10_is_buzz(self):
        result = fizzbuzz(10)
        self.assertEqual(result, 'buzz')

    def test_fizzbuzz_of_15_is_fizzbuzz(self):
        result = fizzbuzz(15)
        self.assertEqual(result, 'fizzbuzz')

    def test_fizzbuzz_of_0_is_fizzbuzz(self):
        result = fizzbuzz(0)
        self.assertEqual(result, 'fizzbuzz')

    def test_fizzbuzz_of_character_raise_exception(self):
        with self.assertRaises(TypeError):
            fizzbuzz('abc')


class TestLoadFizzBuzz(unittest.TestCase):

    def test_load_test_csv_got_csv_content_as_array(self):
        result = load_fizzbuzz('tests/test.csv')
        self.assertEqual(result, [0, 1, 2, 3, 5, 6, 15, 29, 30, 100])


class TestConvertResultToStr(unittest.TestCase):
    def test_convert_array_to_comma_separated_string(self):
        result = convert_result([1, 2, 'fizz'])
        self.assertEqual(result, '1, 2, fizz')


class TestSubmitResultToService(unittest.TestCase):

    @patch('requests.post')
    def test_submit_result_send_correct_arg_to_requests_lib(self, mock_method):
        submit_result('1, 2, fizz', 'my_name')
        mock_method.assert_called_with(
            "http://redisaas.herokuapp.com/my_name", "1, 2, fizz"
        )


class TestIntegration(unittest.TestCase):

    @patch('requests.post')
    def test_run_transform_csv_and_submit_result(self, mock_method):
        run('tests/test.csv', 'my_name')
        mock_method.assert_called_with(
            "http://redisaas.herokuapp.com/my_name",
            "fizzbuzz, 1, 2, fizz, buzz, fizz, fizzbuzz, 29, fizzbuzz, buzz"
        )
