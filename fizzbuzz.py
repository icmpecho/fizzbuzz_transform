import csv
import requests


def fizzbuzz(num):
    if num % 15 == 0:
        return 'fizzbuzz'
    if num % 3 == 0:
        return 'fizz'
    if num % 5 == 0:
        return 'buzz'
    return num


def load_fizzbuzz(filename):
    with open(filename, newline='') as csvfile:
        r = csv.reader(csvfile, delimiter=',')
        return __transform_arr(next(r), int)


def __transform_arr(arr, transformer):
    return [transformer(element) for element in arr]


def convert_result(arr):
    return ', '.join(__transform_arr(arr,str))


def submit_result(result, name):
    url = "http://redisaas.herokuapp.com/{}".format(name)
    requests.post(url, result)


def run(filename, endpoint_key):
    arr = load_fizzbuzz(filename)
    result = [fizzbuzz(element) for element in arr]
    result_str = convert_result(result)
    submit_result(result_str, endpoint_key)

if __name__ == '__main__':
    run('input.csv', 'earthz')
